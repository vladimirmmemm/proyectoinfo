-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2020 a las 07:19:00
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_control_horario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_modalidad`
--

CREATE TABLE `tb_modalidad` (
  `M_idModalidad` tinyint(4) NOT NULL,
  `M_descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_proceso0`
--

CREATE TABLE `tb_proceso0` (
  `P0_idProceso0` tinyint(6) NOT NULL,
  `P0_descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_proceso1`
--

CREATE TABLE `tb_proceso1` (
  `P1_idProceso1` tinyint(4) NOT NULL,
  `P1_descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_proceso2`
--

CREATE TABLE `tb_proceso2` (
  `P2_idProceso2` tinyint(4) NOT NULL,
  `P2_descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_puesto`
--

CREATE TABLE `tb_puesto` (
  `P_idPuesto` smallint(6) NOT NULL,
  `P_descripcion` varchar(200) NOT NULL,
  `P_costoXhora` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_registro_tiempo`
--

CREATE TABLE `tb_registro_tiempo` (
  `RT_idRegistroTiempo` int(11) NOT NULL,
  `RT_fechaHoraRegistro` datetime NOT NULL,
  `RT_idUsuario` smallint(6) NOT NULL,
  `RT_idPuesto` smallint(6) NOT NULL,
  `RT_idProceso0` tinyint(4) NOT NULL,
  `RT_idProceso1` tinyint(4) NOT NULL,
  `RT_idProceso2` tinyint(4) NOT NULL,
  `RT_actividad` varchar(200) NOT NULL,
  `RT_producto` varchar(100) NOT NULL,
  `RT_descripcion` varchar(400) NOT NULL,
  `RT_tiempoXhora` tinyint(4) NOT NULL,
  `RT_costoTotalXHora` double NOT NULL,
  `RT_idModalidad` tinyint(4) NOT NULL,
  `RT_observacion` varchar(400) NOT NULL,
  `RT_fechaHoraInicio` datetime NOT NULL,
  `RT_fechaHoraFinal` datetime NOT NULL,
  `RT_ipComputadora` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_tipo_acceso`
--

CREATE TABLE `tb_tipo_acceso` (
  `TA_idTipoAcceso` tinyint(4) NOT NULL,
  `TA_descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `U_idUsuario` smallint(6) NOT NULL,
  `U_nombre` varchar(100) NOT NULL,
  `U_apellidos` varchar(200) NOT NULL,
  `U_correo` varchar(100) NOT NULL,
  `U_clave` varchar(400) NOT NULL,
  `U_idTipoAcceso` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tb_modalidad`
--
ALTER TABLE `tb_modalidad`
  ADD PRIMARY KEY (`M_idModalidad`);

--
-- Indices de la tabla `tb_proceso0`
--
ALTER TABLE `tb_proceso0`
  ADD PRIMARY KEY (`P0_idProceso0`);

--
-- Indices de la tabla `tb_proceso1`
--
ALTER TABLE `tb_proceso1`
  ADD PRIMARY KEY (`P1_idProceso1`);

--
-- Indices de la tabla `tb_proceso2`
--
ALTER TABLE `tb_proceso2`
  ADD PRIMARY KEY (`P2_idProceso2`);

--
-- Indices de la tabla `tb_puesto`
--
ALTER TABLE `tb_puesto`
  ADD PRIMARY KEY (`P_idPuesto`);

--
-- Indices de la tabla `tb_registro_tiempo`
--
ALTER TABLE `tb_registro_tiempo`
  ADD PRIMARY KEY (`RT_idRegistroTiempo`),
  ADD KEY `foreing_key_tb_usuario` (`RT_idUsuario`),
  ADD KEY `foreing_key_tb_puesto` (`RT_idPuesto`),
  ADD KEY `foreing_key_tb_proceso0` (`RT_idProceso0`),
  ADD KEY `foreing_key_tb_proceso1` (`RT_idProceso1`),
  ADD KEY `foreing_key_tb_proceso2` (`RT_idProceso2`),
  ADD KEY `foreing_key_tb_modalidad` (`RT_idModalidad`);

--
-- Indices de la tabla `tb_tipo_acceso`
--
ALTER TABLE `tb_tipo_acceso`
  ADD PRIMARY KEY (`TA_idTipoAcceso`);

--
-- Indices de la tabla `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`U_idUsuario`),
  ADD KEY `foreing_key_tb_tipo_acceso` (`U_idTipoAcceso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tb_modalidad`
--
ALTER TABLE `tb_modalidad`
  MODIFY `M_idModalidad` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_proceso0`
--
ALTER TABLE `tb_proceso0`
  MODIFY `P0_idProceso0` tinyint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_proceso1`
--
ALTER TABLE `tb_proceso1`
  MODIFY `P1_idProceso1` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_proceso2`
--
ALTER TABLE `tb_proceso2`
  MODIFY `P2_idProceso2` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_puesto`
--
ALTER TABLE `tb_puesto`
  MODIFY `P_idPuesto` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_registro_tiempo`
--
ALTER TABLE `tb_registro_tiempo`
  MODIFY `RT_idRegistroTiempo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_tipo_acceso`
--
ALTER TABLE `tb_tipo_acceso`
  MODIFY `TA_idTipoAcceso` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_usuario`
--
ALTER TABLE `tb_usuario`
  MODIFY `U_idUsuario` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tb_registro_tiempo`
--
ALTER TABLE `tb_registro_tiempo`
  ADD CONSTRAINT `foreing_key_tb_modalidad` FOREIGN KEY (`RT_idModalidad`) REFERENCES `tb_modalidad` (`M_idModalidad`),
  ADD CONSTRAINT `foreing_key_tb_proceso0` FOREIGN KEY (`RT_idProceso0`) REFERENCES `tb_proceso0` (`P0_idProceso0`),
  ADD CONSTRAINT `foreing_key_tb_proceso1` FOREIGN KEY (`RT_idProceso1`) REFERENCES `tb_proceso1` (`P1_idProceso1`),
  ADD CONSTRAINT `foreing_key_tb_proceso2` FOREIGN KEY (`RT_idProceso2`) REFERENCES `tb_proceso2` (`P2_idProceso2`),
  ADD CONSTRAINT `foreing_key_tb_puesto` FOREIGN KEY (`RT_idPuesto`) REFERENCES `tb_puesto` (`P_idPuesto`),
  ADD CONSTRAINT `foreing_key_tb_usuario` FOREIGN KEY (`RT_idUsuario`) REFERENCES `tb_usuario` (`U_idUsuario`);

--
-- Filtros para la tabla `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD CONSTRAINT `foreing_key_tb_tipo_acceso` FOREIGN KEY (`U_idTipoAcceso`) REFERENCES `tb_tipo_acceso` (`TA_idTipoAcceso`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
