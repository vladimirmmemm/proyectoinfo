import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios.service'
@Component({
  selector: 'app-admin-colaboradores',
  templateUrl: './admin-colaboradores.component.html',
  styleUrls: ['./admin-colaboradores.component.css']
})
export class AdminColaboradoresComponent implements OnInit {

  usuarios: any = [];

  constructor( private usuariosService: UsuariosService ) { }

  ngOnInit(): void {
    this.usuariosService.getUsuarios().subscribe(
      res => {
        this.usuarios = res;
        console.log(this.usuarios);
      },
      err => {console.log(err);}
    );
  }

}
