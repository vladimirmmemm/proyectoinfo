import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './principal/home/home.component';
import { HistoriaComponent } from './principal/historia/historia.component';
import { EmpresaComponent } from './principal/empresa/empresa.component';
import { RegistroComponent } from './principal/registro/registro.component';
import { LoginComponent } from './principal/login/login.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'principal/historia', component: HistoriaComponent },
  { path: 'principal/empresa', component: EmpresaComponent },
  { path: 'principal/registro', component: RegistroComponent },
  { path: 'principal/login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
