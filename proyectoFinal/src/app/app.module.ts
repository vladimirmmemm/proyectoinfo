import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminColaboradoresComponent } from './admin/admin-colaboradores/admin-colaboradores.component';
import { AdminReportesComponent } from './admin/admin-reportes/admin-reportes.component';
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { HomeComponent } from './principal/home/home.component';
import { FormsModule } from '@angular/forms';
import { EmpresaComponent } from './principal/empresa/empresa.component';
import { HistoriaComponent } from './principal/historia/historia.component';
import { RegistroComponent } from './principal/registro/registro.component';
import { LoginComponent } from './principal/login/login.component';
import { UsuariosService } from './services/usuarios.service';


@NgModule({
  declarations: [
    AppComponent,
    AdminColaboradoresComponent,
    AdminReportesComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    EmpresaComponent,
    HistoriaComponent,
    RegistroComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    UsuariosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
