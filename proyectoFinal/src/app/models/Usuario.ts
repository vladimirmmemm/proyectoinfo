import { Title } from '@angular/platform-browser';

export interface Usuario{
    
    U_idUsuario?: number;
    U_nombre: string;
    U_apellidos: string;
    U_correo: string;
    U_clave: string;
    U_idTipoAcceso: number;

}


