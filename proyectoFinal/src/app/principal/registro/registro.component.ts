import { Component, OnInit, HostBinding, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from '../../services/usuarios.service';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  usuario: Usuario = {
    U_idUsuario: 0,
    U_nombre: '',
    U_apellidos: '',
    U_correo:'',
    U_clave:'',
    U_idTipoAcceso: 1

  };
  
  constructor(private usuariosService:UsuariosService ) { }

  ngOnInit(): void {
  }

  saveNewUsuario(){
    delete this.usuario.U_idUsuario;

    this.usuariosService.saveUsuario(this.usuario).subscribe(
      res =>{
        Swal.fire({
          icon: 'success',
          title: 'Registrado Correctamente',
          showConfirmButton: false,
          timer: 1500
        });
        console.log(res);
      },
      err => console.error(err)
    )
  }

}
